import React, { useState } from 'react';
import { Button, Input, Message, toaster } from 'rsuite';

function App() {
  const [params, setParams] = useState('');

  const styles = {
    padding: 20,
    textAlign: 'center',
  };

  const b1OnClick = () => {
    try {
      if (params?.length > 0) {
        window.location.href = `app://rbih-demo.hyperverge/?txnId=${params}`;
      } else {
        toaster.push(
          <Message type="error" closable>
            Params cannot be empty
          </Message>
        );
      }
    } catch (error) {
      console.log('no app installed');
    }
  };

  return (
    <div style={styles}>
      <Input
        placeholder="Enter params"
        size="sm"
        value={params}
        onChange={(value) => setParams(value)}
        style={{
          width: '50%',
          margin: '2% auto',
        }}
      />
      <Button id="b1" onClick={b1OnClick}>
        Test Facematch
      </Button>
    </div>
  );
}

export default App;
